#ifndef GENERATION_H_
#define GENERATION_H_

#include <random>

#ifdef WINDOWS
#include <cstdlib>
#endif

#include "common.h"

uint8_t GetRandom();
void RandomVector(ByteVector &data);

#endif /* GENERATION_H_ */
