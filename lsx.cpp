#include "lsx.h"
#include "common.h"

void LSX::FormLookupTables()
{
  for (size_t num_of_table = 0; num_of_table < state_size_; num_of_table++)
  {
    for (int x = 0; x < 256; x++)
    {
      for (size_t pos = 0; pos < state_size_; pos++)
      {
        lookup_tables_[num_of_table][x][pos] =
            linear_matrix_.mult_tables_[num_of_table * state_size_ + pos][x];
        lookup_tables_inv_[num_of_table][x][pos] = invlinear_matrix_
            .mult_tables_[num_of_table * state_size_ + pos][x];
      }
    }
  }
}

SBox<uint8_t>& LSX::get_sbox()
{
  return sbox_;
}

SBox<uint8_t>& LSX::get_invsbox()
{
  return invsbox_;
}

Matrix<uint8_t>& LSX::get_linear_matrix()
{
  return linear_matrix_;
}

Matrix<uint8_t>& LSX::get_invlinear_matrix()
{
  return invlinear_matrix_;
}

ByteVector& LSX::get_state()
{
  return state_;
}

ByteVector& LSX::get_master()
{
  return master_key_;
}

Byte2Vector& LSX::get_keys()
{
  return keys_;
}

void LSX::AddRoundKey(const ByteVector &key)
{
  for (size_t i = 0; i < state_size_; i++)
  {
    state_[i] ^= key[i];
  }
}

void LSX::SubBytes()
{
  for (size_t i = 0; i < state_size_; i++)
  {
    state_[i] = sbox_[state_[i]];
  }
}

void LSX::LinearTransform()
{
  ByteVector tmp(state_size_, 0);
  for (size_t i = 0; i < state_size_; i++)
  {
    tmp = lookup_tables_[i][state_[i]] ^ tmp;
  }
  state_ = tmp;
}

void LSX::InvSubBytes()
{
  for (size_t i = 0; i < state_size_; i++)
  {
    state_[i] = invsbox_[state_[i]];
  }
}

void LSX::InvLinearTransform()
{
  ByteVector tmp(state_size_, 0);
  for (size_t i = 0; i < state_size_; i++)
  {
    tmp = lookup_tables_inv_[i][state_[i]] ^ tmp;
  }
  state_ = tmp;
}

void LSX::LSXRound(const ByteVector &key)
{
  AddRoundKey(key);
  SubBytes();
  LinearTransform();
}

void LSX::LSXRound(ByteVector &input, const ByteVector &key)
{
  for (size_t i = 0; i < input.size(); i++)
  {
    input[i] ^= key[i];
    input[i] = sbox_[input[i]];
  }
  ByteVector tmp(state_size_, 0);
  for (size_t i = 0; i < state_size_; i++)
  {
    tmp = lookup_tables_[i][input[i]] ^ tmp;
  }
  input = tmp;
}

void LSX::InvLSXRound(const ByteVector &key)
{
  AddRoundKey(key);
  InvLinearTransform();
  InvSubBytes();
}

void LSX::KeySchedule(const ByteVector &master)
{
  Byte2Vector C(4, ByteVector(state_size_, 0));
  for (int i = 0; i < 4; i++)
  {
    C[i].back() += (i + 1);
    state_ = C[i];
    LinearTransform();
    C[i] = state_;
  }

  std::copy(master.begin(), master.begin() + 16, keys_[0].begin());
  std::copy(master.begin() + 16, master.end(), keys_[1].begin());

  ByteVector dub, l0, r0;
  l0 = keys_[1];
  r0 = keys_[0];

  for (int nk = 0; nk < 2; nk++)
  {
    for (int nc = 0; nc < 2; nc++)
    {
      dub = r0;
      LSXRound(r0, C[nc + 2*nk]);
      r0 = r0 ^ l0;
      l0 = dub;
    }
    keys_[2*nk+2] = r0;
    keys_[2*nk+3] = l0;
  }
}

void LSX::Show()
{
  for (size_t i = 0; i < state_size_; i++)
  {
    std::cout << std::hex << std::setfill('0') << std::setw(2)
              << static_cast<int>(state_[i]);
  }
  std::cout << std::endl;
}
