#include "common.h"

void Show(const Byte2Vector &a)
{
  for (size_t i = 0; i < a.size(); i++)
  {
    for (size_t j = 0; j < a[i].size(); j++)
    {
      std::cout << std::hex << std::setfill('0') << std::setw(2)
                << static_cast<int>(a[i][j]);
    }
    std::cout << std::endl;
  }
}

void Show(const ByteVector &a)
{
  for (size_t i = 0; i < a.size(); i++)
  {
    std::cout << std::hex << std::hex << std::setfill('0') << std::setw(2)
              << static_cast<int>(a[i]);
  }
  std::cout << std::endl;
}

const ByteVector operator^(const ByteVector& left, const ByteVector& right)
{
  ByteVector c(left.size(), 0);
  for (size_t i = 0; i < left.size(); ++i)
    c[i] = left[i] ^ right[i];
  return c;
}

ByteVector xorv(const ByteVector& left, const ByteVector& right, int size)
{
  ByteVector c(size, 0);
  for (int i = 0; i < size; ++i)
    c[i] = left[i] ^ right[i];
  return c;
}

