#include "difference.h"
#include "common.h"

Difference::Difference(int depth, int width)
{
  depth_ = depth;
  width_ = width;
  diff_ = Byte2Vector(depth, ByteVector(width, 0));
  anchor_ = ByteVector(width, 0);
}

Difference::Difference(const Byte2Vector &data)
{
  depth_ = data.size() - 1;
  anchor_ = data[0];
  width_ = data[0].size();
  for (size_t i = 1; i < data.size(); ++i)
  {
    diff_.push_back(anchor_ ^ data[i]);
  }
}

Byte2Vector Difference::FormData()
{
  Byte2Vector data;
  data.push_back(anchor_);
  for (size_t i = 0; i < diff_.size(); i++)
  {
    data.push_back(diff_[i] ^ anchor_);
  }
  return data;
}

ByteVector& Difference::get_anchor()
{
  return anchor_;
}

int Difference::get_size()
{
  return depth_;
}

ByteVector& Difference::get_row(int i)
{
  return diff_[i];
}

ByteVector Difference::get_column(int column)
{
  ByteVector res;
  for (int i = 0; i < depth_; i++)
  {
    res.push_back(diff_[i][column]);
  }
  return res;
}

void Difference::set_column(const ByteVector &input, int j)
{
  for (int i = 0; i < depth_; i++)
  {
    diff_[i][j] = input[i];
  }
}

void Difference::Show()
{
  for (size_t i = 0; i < diff_.size(); i++)
  {
    for (size_t j = 0; j < diff_[i].size(); j++)
    {
      std::cout << std::hex << std::setfill('0') << std::setw(2)
                << static_cast<int>(diff_[i][j]);
    }
    std::cout << std::endl;
  }
}
