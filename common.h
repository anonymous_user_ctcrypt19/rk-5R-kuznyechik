#ifndef COMMON_H_
#define COMMON_H_

#include <vector>
#include <cstdint>

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <math.h>

using ByteVector = std::vector<uint8_t>;
using Byte2Vector = std::vector< std::vector<uint8_t> >;

void Show(const Byte2Vector &a);
void Show(const ByteVector &a);
const ByteVector operator^(const ByteVector& left, const ByteVector& right);
ByteVector xorv (const ByteVector& left, const ByteVector& right, int size);

#endif /* COMMON_H_ */
