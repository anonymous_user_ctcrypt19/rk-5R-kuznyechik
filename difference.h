#ifndef DIFFERENCE_H_
#define DIFFERENCE_H_

#include "common.h"

class Difference
{
  int depth_;
  int width_;
  Byte2Vector diff_;
  ByteVector anchor_;

 public:
  Difference(int depth, int width);
  Difference(const Byte2Vector& data);
  Byte2Vector FormData();
  ByteVector& get_anchor();
  int get_size();
  ByteVector& get_row(int i);
  ByteVector get_column(int column);
  void set_column(const ByteVector &input, int j);
  void Show();
};

#endif /* DIFFERENCE_H_ */
