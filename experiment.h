#ifndef EXPERIMENT_H_
#define EXPERIMENT_H_

#include "difference.h"
#include "lsx.h"
#include "sbox.h"
#include "generation.h"
#include "common.h"

const int kGFSize = 256;
const int kDifferenceDepth = 255;
const int kNumRelKeys = 256;

class Experiment
{
  int rounds_;
  int block_size_;
  LSX cipher_;

  Byte2Vector GetPossibleDiffS(const ByteVector& diff_column,
                               SBox<uint8_t>& sbox);
  void InvLinearDiff(Difference& beta, LSX& cipher);
  Difference FormAlphaS(const ByteVector& diff_column);
  void LinearDiff(Difference& alpha, LSX& cipher);
  ByteVector FormKappa();
  std::vector<Difference> FormKappaLS(const Byte2Vector& kappa_s);
  std::vector<Difference> FormBetas(std::vector<Difference>& kappa_ls);
  Byte2Vector FormBetaXorSK(ByteVector diff_column);

  bool IsZero(const ByteVector& diff_column);
  bool IsZeroColumn(const Byte2Vector& pd);
  bool IsZeroBeta(Difference& beta);

  std::vector<Difference> FindEquivClass(std::vector<Difference>& betas);
  Byte2Vector FormKappaSLS(std::vector<Difference>& kappa_ls, int column);
  void FormSLSXorSLS(const Byte2Vector& kappa_sls,
                     std::vector<uint64_t>& connect_set);
  uint64_t FromVecTo64(const ByteVector& input);

  std::vector<Byte2Vector> InvSboxAllLists(Difference& beta);
  std::vector<Byte2Vector> InvSboxAllListsNoMul(Difference& beta);
  void VectorMulC(ByteVector& vec, uint8_t c);

  void CommonDifferences(int depth, const ByteVector& elem,
                         const std::vector<Byte2Vector>& possible_diff,
                         std::vector<int> nums,
                         const std::vector<uint64_t>& connect_set,
                         std::vector<std::vector<int> >& solutions);

  Difference FormDifferenceFromListsElems(
      const std::vector<int>& nums, const std::vector<Byte2Vector>& lists);
  ByteVector BrutePreKey(const std::vector<Difference>& final_solutions,
                         Difference& beta);
  void BrutePreKey(const std::vector<Difference>& final_solutions,
                         Difference& beta, std::vector<ByteVector>& prekeys, std::vector<ByteVector>& s_kaps);
  void KeyRecovery(ByteVector& key, Difference& beta);

  void ShowParams();

  ByteVector RecoverMaster(const ByteVector& k5, const ByteVector& k6);

 public:
  Experiment(int rounds, int block_size, LSX cipher);
  ~Experiment();
  void Run();

};

#endif /* EXPERIMENT_H_ */
