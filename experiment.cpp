#include "experiment.h"
#include "matrix.h"
#include "sbox.h"
#include "difference.h"
#include "lsx.h"
#include "common.h"

Experiment::Experiment(int rounds, int block_size, LSX cipher)
    : rounds_(rounds),
      block_size_(block_size),
      cipher_(cipher)
{
}

Experiment::~Experiment()
{

}

Byte2Vector Experiment::GetPossibleDiffS(const ByteVector& diff_column,
                                         SBox<uint8_t>& sbox)
{
  Byte2Vector ddt;
  ddt.reserve(kGFSize);
  for (int x = 0; x < kGFSize; x++)
  {
    ByteVector tmp(diff_column.size(), 0);
    for (size_t i = 0; i < diff_column.size(); i++)
    {
      tmp[i] = sbox[x ^ diff_column[i]] ^ sbox[x];
    }
    ddt.push_back(tmp);
  }
  return ddt;
}

Difference Experiment::FormAlphaS(const ByteVector& diff_column)
{
  Difference alpha_s(kDifferenceDepth, block_size_);
  alpha_s.set_column(diff_column, (block_size_ - 1));
  return alpha_s;
}

void Experiment::LinearDiff(Difference& alpha, LSX& cipher)
{
  for (int i = 0; i < alpha.get_size(); i++)
  {
    cipher.get_state() = alpha.get_row(i);
    cipher.LinearTransform();
    alpha.get_row(i) = cipher.get_state();
  }
}

bool Experiment::IsZero(const ByteVector& diff_column)
{
  int sum = 0;
  for (unsigned int i = 0; i < diff_column.size(); i++)
  {
    sum ^= diff_column[i];
  }
  if (sum == 0)
    return true;
  else
    return false;
}

bool Experiment::IsZeroColumn(const Byte2Vector& pd)
{
  for (const auto& diff : pd)
  {
    if (IsZero(diff) == true)
    {
      return true;
    }
  }
  return false;
}

Byte2Vector Experiment::FormBetaXorSK(ByteVector diff_column)
{
  Byte2Vector result;
  result.reserve(size_t(2) << 15);  //2^8 * 2^8
  Byte2Vector kappa_s = GetPossibleDiffS(FormKappa(), cipher_.get_sbox());
  for (const auto& pd : kappa_s)
  {
    result.push_back(pd ^ diff_column);
  }
  return result;
}

bool Experiment::IsZeroBeta(Difference& beta)
{
  InvLinearDiff(beta, cipher_);
  for (int column = 0; column < block_size_ - 1; column++)
  {
    auto pd = GetPossibleDiffS(beta.get_column(column), cipher_.get_invsbox());
    if (IsZeroColumn(pd) == false)
    {
      return false;
    }
  }
  Byte2Vector beta_xor_sk = FormBetaXorSK(beta.get_column(block_size_ - 1));
  Byte2Vector pd;
  for (size_t i = 0; i < beta_xor_sk.size(); i++)
  {
    auto pd_summ = GetPossibleDiffS(beta_xor_sk[i], cipher_.get_invsbox());
    pd.insert(pd.end(), pd_summ.begin(), pd_summ.end());
  }
  if (IsZeroColumn(pd) == false)
  {
    return false;
  }
  return true;
}

void Experiment::InvLinearDiff(Difference& beta, LSX& cipher)
{
  for (int i = 0; i < beta.get_size(); i++)
  {
    cipher.get_state() = beta.get_row(i);
    cipher.InvLinearTransform();
    beta.get_row(i) = cipher.get_state();
  }
}

ByteVector Experiment::FormKappa()
{
  ByteVector kappa;
  kappa.reserve(kDifferenceDepth);
  for (int i = 1; i < kGFSize; i++)
  {
    kappa.push_back(i);
  }
  return kappa;
}

std::vector<Difference> Experiment::FormKappaLS(const Byte2Vector& kappa_s)
{
  std::vector<Difference> kappa_ls;
  kappa_ls.reserve(size_t(2) << 15);  // 2^8 * 2^8
  for (const auto& pd : kappa_s)
  {
    auto diff = FormAlphaS(pd);
    LinearDiff(diff, cipher_);
    kappa_ls.push_back(diff);
  }
  return kappa_ls;
}

std::vector<Difference> Experiment::FormBetas(std::vector<Difference>& kappa_ls)
{
  std::vector<Difference> betas;
  betas.reserve(kGFSize);
  for (size_t i = 0; i < kappa_ls.size(); i++)
  {
    kappa_ls[i].get_anchor() = std::vector<uint8_t>(
        cipher_.get_master().begin() + 16, cipher_.get_master().end());
    auto k2_keys = kappa_ls[i].FormData();
    Byte2Vector beta(kNumRelKeys, ByteVector(block_size_, 0));
    for (int key = 0; key < kNumRelKeys; key++)
    {
      ByteVector ms_key(cipher_.get_master().begin(),
                        cipher_.get_master().begin() + 16);
      ms_key.back() ^= key;
      ms_key.insert(ms_key.end(), k2_keys[key].begin(), k2_keys[key].end());
      cipher_.KeySchedule(ms_key);
      ByteVector C1 = {0x6e, 0xa2, 0x76, 0x72, 0x6c, 0x48, 0x7a, 0xb8, 0x5d, 0x27, 0xbd, 0x10, 0xdd, 0x84, 0x94, 0x01};
      cipher_.get_state() = C1;
      for (int k = 0; k < rounds_; k++)
      {
        cipher_.LSXRound(cipher_.get_keys()[k]);
      }
      cipher_.AddRoundKey(cipher_.get_keys()[rounds_]);
      beta[key] = cipher_.get_state();
    }
    betas.push_back(Difference(beta));
  }
  return betas;
}

void Experiment::ShowParams()
{
  std::cout << "Rounds : " << rounds_ << std::endl;
  std::cout << "Block size : " << block_size_ << " bytes" << std::endl;
  std::cout << "Master key : ";
  Show(cipher_.get_master());
  std::cout << "Round keys : " << std::endl;
  cipher_.KeySchedule(cipher_.get_master());
  auto keys = cipher_.get_keys();
  for (size_t i = 0; i < keys.size(); i++)
  {
    std::cout << "Key " << std::dec << i+1 <<" : ";
    Show(keys[i]);
  }
}

std::vector<Difference> Experiment::FindEquivClass(
    std::vector<Difference>& betas)
{
  std::vector<Difference> classes;
  for (size_t i = 0; i < betas.size(); i++)
  {
    if (IsZeroBeta(betas[i]) == true)
    {
      std::cout << "Equivalence class x = 0x" << i << std::endl;
      classes.push_back(betas[i]);
    }
  }
  return classes;
}

Byte2Vector Experiment::FormKappaSLS(std::vector<Difference>& kappa_ls,
                                     int column)
{
  Byte2Vector kappa_sls;
  for (size_t i = 0; i < kappa_ls.size(); i++)
  {
    auto pd_kls = GetPossibleDiffS(kappa_ls[i].get_column(column),
                                   cipher_.get_sbox());
    kappa_sls.insert(kappa_sls.end(), pd_kls.begin(), pd_kls.end());
  }
  return kappa_sls;
}

uint64_t Experiment::FromVecTo64(const ByteVector& input)
{
  uint64_t res = 0;
  for (int i = 0; i < 8; i++)
  {
    res |= (uint64_t) (input[i]) << (56 - 8 * i);
  }
  return res;
}

void Experiment::FormSLSXorSLS(const Byte2Vector& kappa_sls,
                               std::vector<uint64_t>& connect_set)
{
  connect_set.reserve(size_t(2) << 30);  //2^31
  Byte2Vector keys = kappa_sls;
  Difference kappa_d = FormAlphaS(FormKappa());
  InvLinearDiff(kappa_d, cipher_);
  uint64_t linv_kappa = FromVecTo64(kappa_d.get_column(0)); //connect 1st column
  connect_set.push_back(uint64_t(0));
  for (size_t i = 0; i < kappa_sls.size() - 1; i++)
  {
    for (size_t j = i + 1; j < keys.size(); j++)
    {
      auto summ = xorv(kappa_sls[i], keys[j], 8);
      connect_set.push_back(FromVecTo64(summ) ^ linv_kappa);
    }
  }
  std::cout << "Formed SLS(K) xor SLS(K)..." << std::endl;
  std::cout << "Connect set size : " << std::dec << connect_set.size()
            << std::endl;
  std::sort(connect_set.begin(), connect_set.end());
  std::cout << "Sorted SLS(K) xor SLS(K)..." << std::endl;
  std::cout << std::endl;
}

void Experiment::VectorMulC(ByteVector& vec, uint8_t c)
{
  for (size_t i = 0; i < vec.size(); i++)
    vec[i] = cipher_.get_invlinear_matrix().mult_tables_[c][vec[i]];
}

std::vector<Byte2Vector> Experiment::InvSboxAllLists(Difference& beta)
{
  size_t list_size = 1;
  std::vector<Byte2Vector> pd_cont(block_size_);
  auto& sbox = cipher_.get_invsbox();
  for (int i = 0; i < block_size_ - 1; i++)
  {
    auto matrix_elem = block_size_ * i;
    auto all_possible = GetPossibleDiffS(beta.get_column(i), sbox);
    for (auto& elem : all_possible)
    {
      if (IsZero(elem) == true)
      {

        VectorMulC(elem, matrix_elem);
        pd_cont[i].push_back(elem);
      }
    }
    list_size *= pd_cont[i].size();
  }
  Byte2Vector beta_xor_sk = FormBetaXorSK(beta.get_column(block_size_ - 1));
  auto matrix_elem = block_size_ * (block_size_ - 1);
  for (size_t i = 0; i < beta_xor_sk.size(); i++)
  {
    auto all_possible = GetPossibleDiffS(beta_xor_sk[i], sbox);
    for (auto& elem : all_possible)
    {
      if (IsZero(elem) == true)
      {
        VectorMulC(elem, matrix_elem);
        pd_cont[block_size_ - 1].push_back(elem);
      }
    }
  }
  list_size *= pd_cont[block_size_ - 1].size();
  std::cout << "Generated list of beta`s possible differences..."
            << std::endl;
  std::cout << "Possible differences : " << std::dec << list_size << " [2**"
            << std::log2(list_size) << "]" << std::endl;
  return pd_cont;
}

std::vector<Byte2Vector> Experiment::InvSboxAllListsNoMul(Difference& beta)
{
  std::vector<Byte2Vector> pd_cont(block_size_);
  auto& sbox = cipher_.get_invsbox();
  for (int i = 0; i < block_size_ - 1; i++)
  {
    auto all_possible = GetPossibleDiffS(beta.get_column(i), sbox);
    for (auto& elem : all_possible)
    {
      if (IsZero(elem) == true)
      {
        pd_cont[i].push_back(elem);
      }
    }
  }
  Byte2Vector beta_xor_sk = FormBetaXorSK(beta.get_column(block_size_ - 1));
  for (size_t i = 0; i < beta_xor_sk.size(); i++)
  {
    auto all_possible = GetPossibleDiffS(beta_xor_sk[i], sbox);
    for (auto& elem : all_possible)
    {
      if (IsZero(elem) == true)
      {
        pd_cont[block_size_ - 1].push_back(elem);
      }
    }
  }
  return pd_cont;
}

void Experiment::CommonDifferences(
    int depth, const ByteVector& elem,
    const std::vector<Byte2Vector>& possible_diff, std::vector<int> nums,
    const std::vector<uint64_t>& connect_set,
    std::vector<std::vector<int> >& solutions)
{
  if (depth == block_size_)
  {
    if (std::binary_search(connect_set.begin(), connect_set.end(),
                           FromVecTo64(elem)))
    {
      solutions.push_back(nums);
    }
    return;
  }
  for (size_t i = 0; i < possible_diff[depth].size(); i++)
  {
    nums.push_back(i);
    CommonDifferences(depth + 1, possible_diff[depth][i] ^ elem, possible_diff,
                      nums, connect_set, solutions);
    nums.pop_back();
  }
}

Difference Experiment::FormDifferenceFromListsElems(
    const std::vector<int>& nums, const std::vector<Byte2Vector>& lists)
{
  Difference beta_inv_s(kDifferenceDepth, block_size_);
  for (int i = 0; i < block_size_; i++)
  {
    beta_inv_s.set_column(lists[i][nums[i]], i);
  }
  return beta_inv_s;
}

ByteVector Experiment::BrutePreKey(
    const std::vector<Difference>& final_solutions, Difference& beta)
{
  ByteVector key;
  auto& sbox_inv = cipher_.get_invsbox();
  for (size_t i = 0; i < final_solutions.size(); i++)
  {
    Difference candidat = final_solutions[i];
    for (int k = 0; k < block_size_; k++)
    {
      ByteVector diff_column = candidat.get_column(k);
      ByteVector beta_column = beta.get_column(k);
      for (int x = 0; x < kGFSize; x++)
      {
        ByteVector tmp(kDifferenceDepth, 0);
        bool isWrong = false;
        for (int i = 0; i < kDifferenceDepth; i++)
        {
          tmp[i] = sbox_inv[x ^ beta_column[i]] ^ sbox_inv[x];
          if (tmp[i] != diff_column[i])
          {
            isWrong = true;
            break;
          }
        }
        if (isWrong == false)
        {
          key.push_back(x);
          break;
        }
      }
    }
  }
  return key;
}

void Experiment::BrutePreKey(const std::vector<Difference>& final_solutions,
                             Difference& beta, std::vector<ByteVector>& prekeys,
                             std::vector<ByteVector>& s_kaps)
{
  auto& sbox_inv = cipher_.get_invsbox();
  for (size_t i = 0; i < final_solutions.size(); i++)
  {
    ByteVector prekey;
    ByteVector s_kap;
    Difference candidat = final_solutions[i];
    for (int k = 0; k < block_size_ - 1; k++)
    {
      ByteVector diff_column = candidat.get_column(k);
      ByteVector beta_column = beta.get_column(k);
      for (int x = 0; x < kGFSize; x++)
      {
        ByteVector tmp(kDifferenceDepth, 0);
        bool isWrong = false;
        for (int i = 0; i < kDifferenceDepth; i++)
        {
          tmp[i] = sbox_inv[x ^ beta_column[i]] ^ sbox_inv[x];
          if (tmp[i] != diff_column[i])
          {
            isWrong = true;
            break;
          }
        }
        if (isWrong == false)
        {
          prekey.push_back(x);
          break;
        }
      }
    }
    ByteVector diff_column = candidat.get_column(block_size_ - 1);
    Byte2Vector beta_xor_sk = FormBetaXorSK(beta.get_column(block_size_ - 1));
    for (size_t sk = 0; sk < beta_xor_sk.size(); sk++)
    {
      ByteVector beta_column = beta_xor_sk[sk];
      for (int x = 0; x < kGFSize; x++)
      {
        ByteVector tmp(kDifferenceDepth, 0);
        bool isWrong = false;
        for (int i = 0; i < kDifferenceDepth; i++)
        {
          tmp[i] = sbox_inv[x ^ beta_column[i]] ^ sbox_inv[x];
          if (tmp[i] != diff_column[i])
          {
            isWrong = true;
            break;
          }
        }
        if (isWrong == false)
        {
          s_kap = beta_xor_sk[sk] ^ beta.get_column(block_size_ - 1);
          prekey.push_back(x);
          break;
        }
      }
    }
    prekeys.push_back(prekey);
    s_kaps.push_back(s_kap);
  }
}

void Experiment::KeyRecovery(ByteVector& key, Difference& beta)
{
  ByteVector anchor = beta.get_anchor();
  cipher_.get_state() = anchor;
  cipher_.InvLinearTransform();
  anchor = key ^ cipher_.get_state();
  cipher_.get_state() = anchor;
  cipher_.LinearTransform();
  key = cipher_.get_state();
}

ByteVector Experiment::RecoverMaster(const ByteVector& k5, const ByteVector& k6)
{
  Byte2Vector C(4, ByteVector(block_size_, 0));
  for (int i = 0; i < 4; i++)
  {
    C[i].back() += (i + 1);
    cipher_.get_state() = C[i];
    cipher_.LinearTransform();
    C[i] = cipher_.get_state();
  }

  ByteVector dub, l0, r0;
  l0 = k5;
  r0 = k6;

  for (int nk = 1; nk >= 0; nk--)
  {
    for (int nc = 1; nc >= 0; nc--)
    {
      dub = r0;
      cipher_.LSXRound(r0, C[nc + 2 * nk]);
      r0 = r0 ^ l0;
      l0 = dub;
    }
  }

  ByteVector master;
  master.insert(master.end(), l0.begin(), l0.end());
  master.insert(master.end(), r0.begin(), r0.end());

  return master;
}

void Experiment::Run()
{
  //Get ciphertexts
  ShowParams();
  std::cout << "----------------------------------------------" << std::endl;
  std::cout << std::endl;
  ByteVector kappa = FormKappa();
  Byte2Vector kappa_s = GetPossibleDiffS(kappa, cipher_.get_sbox());
  std::vector<Difference> kappa_ls = FormKappaLS(kappa_s);
  std::vector<Difference> ciph_txts = FormBetas(kappa_ls);

  //Find "right" classes
  std::vector<Difference> beta_classes = FindEquivClass(ciph_txts);
  std::cout << "Num of correct beta`s : " << std::dec << beta_classes.size()
            << std::endl;
  std::cout << std::endl;

  //Form connect set
  Byte2Vector kappa_sls = FormKappaSLS(kappa_ls, 0);
  std::vector<uint64_t> connect_set;
  FormSLSXorSLS(kappa_sls, connect_set);

  //Find solutions and keys for each "right" class
  for (size_t i = 0; i < beta_classes.size(); i++)
  {
    std::cout << "Class : " << std::dec << i + 1 << std::endl;
    std::cout << "----------------------------------------------" << std::endl;

    //Find solutions
    std::vector<Byte2Vector> lists = InvSboxAllLists(beta_classes[i]);
    std::vector<std::vector<int>> solutions;
    std::vector<int> nums;
    ByteVector zeroes(kDifferenceDepth, 0);
    CommonDifferences(0, zeroes, lists, nums, connect_set, solutions);
    std::cout << "Solutions : " << solutions.size() << std::endl;

    if (solutions.size() == 0)
      continue;

    lists = InvSboxAllListsNoMul(beta_classes[i]);
    std::vector<Difference> final_solutions;
    for (size_t j = 0; j < solutions.size(); j++)
    {
      Difference sol = FormDifferenceFromListsElems(solutions[j], lists);
      final_solutions.push_back(sol);
    }

    //Find all K6' and concrete S(kappa)
    std::vector<ByteVector> keys6;
    std::vector<ByteVector> s_kaps;
    BrutePreKey(final_solutions, beta_classes[i], keys6, s_kaps);
    LinearDiff(beta_classes[i], cipher_);

    //For all K6 and concrete S(kappa) recovery K5 and master key.
    for (size_t k = 0; k < keys6.size(); k++)
    {
      std::cout << "Solution " << std::dec << k + 1 << std::endl;
      KeyRecovery(keys6[k], beta_classes[i]);
      Difference ls_kap_real = FormAlphaS(s_kaps[k]);
      LinearDiff(ls_kap_real, cipher_);

      ls_kap_real.get_anchor() = keys6[k];
      auto delta_keys = ls_kap_real.FormData();
      auto ciphertexts = beta_classes[i].FormData();
      Byte2Vector beta5(kNumRelKeys, ByteVector(block_size_, 0));
      for (int key = 0; key < kNumRelKeys; key++)
      {
        cipher_.get_state() = ciphertexts[key];
        cipher_.InvLSXRound(delta_keys[key]);
        cipher_.InvLinearTransform();
        beta5[key] = cipher_.get_state();
      }

      Difference beta_5r(beta5);

      Difference linv_kappa = FormAlphaS(FormKappa());
      InvLinearDiff(linv_kappa, cipher_);

      Difference delta_k5(kDifferenceDepth, block_size_);
      Difference delta_p5(kDifferenceDepth, block_size_);

      for (int num_sb = 0; num_sb < block_size_; num_sb++)
      {
        ByteVector linv_kappa_col = linv_kappa.get_column(num_sb);
        ByteVector beta_5r_col = beta_5r.get_column(num_sb);
        Byte2Vector delta_k5_col = GetPossibleDiffS(
            ls_kap_real.get_column(num_sb), cipher_.get_sbox());
        Byte2Vector kappa_sls_col = FormKappaSLS(kappa_ls, num_sb);
        for (size_t pd = 0; pd < delta_k5_col.size(); pd++)
        {
          auto possible_key = delta_k5_col[pd] ^ linv_kappa_col;
          auto sum = beta_5r_col ^ possible_key;
          auto it = std::find(kappa_sls_col.begin(), kappa_sls_col.end(), sum);
          if (it != kappa_sls_col.end())
          {
            delta_k5.set_column(possible_key, num_sb);
          }
        }
        delta_p5.set_column(
            delta_k5.get_column(num_sb) ^ beta_5r.get_column(num_sb), num_sb);
      }

      ByteVector key5 = BrutePreKey(kappa_ls, delta_p5);
      cipher_.get_state() = key5 ^ beta_5r.get_anchor();
      cipher_.LinearTransform();
      key5 = cipher_.get_state();

      ByteVector recovered_key = RecoverMaster(key5, keys6[k]);

      cipher_.KeySchedule(recovered_key);
      cipher_.get_state() = ciphertexts[0];
      for (int t = rounds_; t > 0; t--)
      {
        cipher_.InvLSXRound(cipher_.get_keys()[t]);
      }
      cipher_.AddRoundKey(cipher_.get_keys()[0]);
      ByteVector C1 = {0x6e, 0xa2, 0x76, 0x72, 0x6c, 0x48, 0x7a, 0xb8, 0x5d,
          0x27, 0xbd, 0x10, 0xdd, 0x84, 0x94, 0x01};

      //If K6 and S(kappa) was correct - it is true
      if (cipher_.get_state() == C1)
      {
        std::cout << "----------------------------------------------"
                  << std::endl;
        std::cout << "Key 6 : ";
        Show(keys6[k]);
        std::cout << "----------------------------------------------"
                  << std::endl;
        std::cout << "----------------------------------------------"
                  << std::endl;
        std::cout << "Key 5 : ";
        Show(key5);
        std::cout << "----------------------------------------------"
                  << std::endl;
        std::cout << "----------------------------------------------"
                  << std::endl;
        std::cout << "Master key : ";
        Show(recovered_key);
        std::cout << "----------------------------------------------"
                  << std::endl;
      }
      else
      {
        std::cout << "Wrong Key 6 / S(kappa) / Key 5" << std::endl;
      }
      std::cout << std::endl;
    }
  }
}
