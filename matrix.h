#ifndef MATRIX_H_
#define MATRIX_H_

#include <initializer_list>
#include "common.h"

template<typename Field>
class Matrix
{
  std::vector<Field> matrix_;
  uint8_t poly_;

  uint8_t gf_multiply(uint8_t a, uint8_t b)
  {
    uint8_t acc = 0x00;
    for (uint8_t i = 0; i < 8; i++)
    {
      if (b & 0x01)
      {
        acc ^= a;
      }
      uint8_t msb = a & 0x80;
      a <<= 1;
      if (msb)
      {
        a ^= poly_;
      }
      b >>= 1;
    }
    return acc;
  }

 public:
  std::vector<std::vector<Field>> mult_tables_;

  std::vector<std::vector<Field>> brut_mult_tables_;

  std::vector<Field> inverse_table_;

  Matrix(std::initializer_list<Field> list, uint8_t poly)
      : matrix_(list),
        poly_(poly)
  {
    mult_tables_ = std::vector<std::vector<Field> >(
        matrix_.size(), std::vector<Field>(2 << (8 * sizeof(Field)), 0));
    brut_mult_tables_ = std::vector<std::vector<Field> >(
        2 << (8 * sizeof(Field)),
        std::vector<Field>(2 << (8 * sizeof(Field)), 0));
    inverse_table_ = std::vector<Field>(2 << (8 * sizeof(Field)), 0);
    FormMultTables();
    FormBrutMultTables();
  }

  Matrix()
      : poly_(0xc3),
        mult_tables_(
            std::vector<std::vector<Field> >(
                2 << (8 * sizeof(Field)),
                std::vector<Field>(2 << (8 * sizeof(Field)), 0)))

  {

  }

  Matrix(std::vector<Field> matrix, uint8_t poly)
      : matrix_(matrix),
        poly_(poly)
  {
    mult_tables_ = std::vector<std::vector<Field> >(
        matrix_.size(), std::vector<Field>(2 << (8 * sizeof(Field)), 0));
    brut_mult_tables_ = std::vector<std::vector<Field> >(
        2 << (8 * sizeof(Field)),
        std::vector<Field>(2 << (8 * sizeof(Field)), 0));
    inverse_table_ = std::vector<Field>(2 << (8 * sizeof(Field)), 0);
    FormMultTables();
    FormBrutMultTables();
  }

  uint8_t& get_poly()
  {
    return poly_;
  }

  Field& operator[](size_t n)
  {
    return matrix_[n];
  }

  void FormMultTables()
  {
    for (size_t i = 0; i < matrix_.size(); i++)
    {
      for (int x = 0; x < 2 << (8 * sizeof(Field)); x++)
      {
        mult_tables_[i][x] = gf_multiply(matrix_[i], x);
      }
    }
  }

  void Show(int bs)
  {
    for (size_t i = 0; i < bs; i++)
    {
      for (size_t j = 0; j < bs; j++)
      {
        std::cout << std::hex << std::setfill('0') << std::setw(2)
            << static_cast<int>(matrix_[i * bs + j]);
      }
      std::cout << std::endl;
    }
  }

  void FormBrutMultTables()
  {
    for (size_t i = 0; i < 2 << (8 * sizeof(Field)); i++)
    {
      for (int x = 0; x < 2 << (8 * sizeof(Field)); x++)
      {
        brut_mult_tables_[i][x] = gf_multiply(i, x);
        if (brut_mult_tables_[i][x] == 1)
          inverse_table_[i] = x;
      }
    }
  }

  Matrix<Field> GenerateInverse()
  {
    auto matr = matrix_;
    std::vector<Field> e_matrix(matr.size(), 0);
    int block_size = std::sqrt(matr.size());

    for (auto i = 0; i < block_size; i++)
    {
      e_matrix[i * block_size + i] = 1;
    }

    for (auto i = 0; i < block_size; i++)
    {
      auto inverse = inverse_table_[matr[i * block_size + i]];
      if (inverse == 0)
        throw("qq");
      for (auto t = 0; t < block_size; t++)
      {
        matr[i * block_size + t] =
            brut_mult_tables_[matr[i * block_size + t]][inverse];
        e_matrix[i * block_size + t] = brut_mult_tables_[e_matrix[i * block_size
            + t]][inverse];
      }
      for (auto k = i + 1; k < block_size; k++)
      {
        auto first_in_row = matr[k * block_size + i];
        for (auto j = 0; j < block_size; j++)
        {

          matr[k * block_size + j] ^=
              brut_mult_tables_[matr[i * block_size + j]][first_in_row];
          e_matrix[k * block_size + j] ^= brut_mult_tables_[e_matrix[i
              * block_size + j]][first_in_row];
        }
      }
    }

    for (auto i = block_size - 1; i > -1; i--)
    {
      for (auto k = 0; k < i; k++)
      {
        auto first_in_row = matr[k * block_size + i];
        for (auto j = 0; j < block_size; j++)
        {

          matr[k * block_size + j] ^=
              brut_mult_tables_[matr[i * block_size + j]][first_in_row];
          e_matrix[k * block_size + j] ^= brut_mult_tables_[e_matrix[i
              * block_size + j]][first_in_row];
        }
      }
    }

    Matrix<Field> inv_matrix(e_matrix, get_poly());
    return inv_matrix;
  }

};

#endif /* MATRIX_H_ */
