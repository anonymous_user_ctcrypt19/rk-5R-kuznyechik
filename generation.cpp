#include "generation.h"

std::random_device seed;
std::mt19937_64 rng(seed());
std::uniform_int_distribution<uint8_t> dst(0, 0xff);

uint8_t GetRandom()
{
  #ifdef WINDOWS
  return rand();
  #endif
  return dst(rng);
}

void RandomVector(ByteVector &data)
{
  for (size_t i = 0; i < data.size(); i++)
  {
    data[i] = GetRandom();
  }
}

